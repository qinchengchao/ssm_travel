package com.qin.travel.utils;

import com.qin.travel.domain.Cart;
import com.qin.travel.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;

@Component
//购物车的工具类，在Redis中操作购物车数据
public class CartUtils {

    @Autowired
    private RedisTemplate<String, Cart> redisTemplate;

    //设置购物车数据到缓存中
    public void setCart(User user, Cart cart) {
        //获得Redis对象
        ValueOperations<String, Cart> opsForValue = redisTemplate.opsForValue();
        opsForValue.set("cart_" + user.getUsername(), cart);
    }

    //获取购物车数据
    public Cart getCart(User user) {
        ValueOperations<String, Cart> opsForValue = redisTemplate.opsForValue();
        Cart cart = opsForValue.get("cart_" + user.getUsername());
        //如果用户还没有添加过
        if (cart == null) {
            cart = new Cart();
            opsForValue.set("cart_" + user.getUsername(), cart);
        }

        return cart;
    }

    //删除购物车数据
    public void removeCart(User user) {
        redisTemplate.delete("cart_" + user.getUsername());
    }
}

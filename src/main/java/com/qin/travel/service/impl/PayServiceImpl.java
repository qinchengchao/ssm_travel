package com.qin.travel.service.impl;

import com.qin.travel.dao.OrderDao;
import com.qin.travel.dao.OrderItemDao;
import com.qin.travel.domain.Order;
import com.qin.travel.service.PayService;
import org.apache.ibatis.annotations.Select;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class PayServiceImpl implements PayService {
    @Autowired
    private OrderDao orderDao;
    @Autowired
    private OrderItemDao orderItemDao;


    @Override
    public void updateOrder(Map<String, String> map) {
        //获取map中的数据
        String oid = map.get("out_trade_no");
        String code = map.get("result_code");

        //判断是否支付成功
        if ("SUCCESS".equals(code)) {
            //支付成功则调用SQL语句，修改数据库中订单的状态
            orderDao.updateOrder(oid);
            orderItemDao.updateOrderItem(oid);
        }
    }

    @Override
    public boolean selectOrder(String oid) {
        Order order = orderDao.selectState(oid);

        if (order.getState() == 1) {
            return true;
        } else {
            return false;
        }
    }
}

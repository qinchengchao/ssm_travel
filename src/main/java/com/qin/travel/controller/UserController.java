package com.qin.travel.controller;

import com.qin.travel.domain.ResultInfo;
import com.qin.travel.domain.User;
import com.qin.travel.service.UserService;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

@RestController
@RequestMapping(path = "/user", produces = "application/json;charset=utf-8")
public class UserController {

    @Autowired
    private UserService userService;

    //注册功能
    @RequestMapping("/registered")
    public ResultInfo registered(@RequestBody Map<String, Object> map) throws InvocationTargetException, IllegalAccessException {
        //得到输入的验证码
        String code = (String) (map.get("code"));
        //得到输入的数据
        Map<String, Object> param = (Map<String, Object>) map.get("user");
        User user = new User();
        BeanUtils.populate(user, param);
        //调用业务方法，对注册的信息进行判断
        return userService.save(user, code);

    }

    //验证用户名是否可用
    @RequestMapping("/username/{username}")
    public boolean testAndVerifyUsername(@PathVariable("username") String username) {
        User byName = userService.findByName(username);
        if (byName == null) {
            return true;
        }

        return false;
    }

    //验证手机号是否可用
    @RequestMapping("/telephone/{telephone}")
    public boolean testAndVerifyTelephone(@PathVariable("telephone") String telephone) {
        boolean flag = userService.findByTelephone(telephone);

        return flag;
    }

    //根据手机号发送短信
    @RequestMapping("/sms/{telephone}")
    public ResultInfo sms(@PathVariable("telephone") String telephone) {
        //调用验证码业务，发送一个验证码
        ResultInfo resultInfo = userService.sendSMS(telephone);

        System.out.println("成功发送短信");
        return resultInfo;

    }

    //发送登录验证码
    @RequestMapping("/send/{username}")
    public ResultInfo sendMessage(@PathVariable("username") String username) {
        //调用业务层方法
        return userService.sendMessage(username);
    }

    //用户登录方法
    @RequestMapping("/login")
    public ResultInfo login(@RequestBody Map<String, String> param, HttpSession session) {

        //获得传入的账户名
        String username = param.get("username");
        //根据用户名获得对象
        User user = userService.findByName(username);
        //判断对象是否为null
        if (user == null) {
            return new ResultInfo(false, "用户名没有注册");
        }

        //调用业务方法，判断密码是否正确
        ResultInfo resultInfo = userService.loginUser(param, user);
        if (resultInfo.getSuccess()) {
            //登录成功，将数据写入session
            session.setAttribute("user", user);
        }
        return resultInfo;
    }

    //验证是否登录
    @RequestMapping("/isLogin")
    public ResultInfo isLogin(HttpSession session) {
        //从session中获取会话域对象
        User user = (User) session.getAttribute("user");
        //判断是否有数据
        if (user == null) {
            return new ResultInfo(false);
        }
        return new ResultInfo(true, user);
    }

    //退出登录
    @RequestMapping("/quit")
    public boolean quit(HttpSession session) {
        session.removeAttribute("user");
        return true;
    }
}

package com.qin.travel.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collection;
import java.util.LinkedHashMap;

//购物车
@Data
public class Cart {

    //购物车中的购物项,String为route的rid
    private LinkedHashMap<String, CartItem> cartItem = new LinkedHashMap<>();

    //购物车中的总数量
    private Integer cartNum;

    //购物车中的总价格
    private Double cartTotal;

    //购物车的总数量等于每一条购物项的和
    public Integer getCartNum() {
        this.cartNum = 0;
        Collection<CartItem> values = this.cartItem.values();
        for (CartItem value : values) {
            this.cartNum += value.getNum();
        }
        return cartNum;
    }

    //购物车的总计等于每一个购物项的小计和
    public Double getCartTotal() {
        this.cartTotal = 0.0;
        Collection<CartItem> values = this.cartItem.values();
        for (CartItem value : values) {
            this.cartTotal += value.getSubTotal();
        }
        return cartTotal;
    }
}

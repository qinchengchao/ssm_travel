package com.qin.travel.service.impl;

import com.qin.travel.dao.AddressDao;
import com.qin.travel.domain.Address;
import com.qin.travel.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AddressServiceImpl implements AddressService {

    @Autowired
    private AddressDao addressDao;

    //根据uid获取到指定用户的所有收货地址
    @Override
    public List<Address> findAddressByUid(Integer uid) {
        //根据uid调用接口执行

        return addressDao.findAddressByUid(uid);
    }
}

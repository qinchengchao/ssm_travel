package com.qin.travel.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.qin.travel.domain.ResultInfo;
import com.qin.travel.domain.User;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter({"/cart/*", "/order/*"})
public class CartFilter implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        //获取session对象，判断用户是否登录
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        //判断用户是否登录
        if (user == null) {
            //设置响应的数据格式，要通过输出流给浏览器返回数据
            response.setHeader("content-type", "application/json;charset=utf-8");
            ResultInfo resultInfo = new ResultInfo(false, "请先登录");
            //获得转工具类
            ObjectMapper objectMapper = new ObjectMapper();
            //将数据转换为json字符串
            String json = objectMapper.writeValueAsString(resultInfo);
            //通过io流，将数据输出到浏览器
            response.getWriter().write(json);
            return;

        }
        //放行
        chain.doFilter(request, response);
    }

    public void init(FilterConfig config) throws ServletException {

    }

}

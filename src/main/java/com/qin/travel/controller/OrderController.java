package com.qin.travel.controller;

import com.qin.travel.dao.AddressDao;
import com.qin.travel.domain.Address;
import com.qin.travel.domain.Order;
import com.qin.travel.domain.ResultInfo;
import com.qin.travel.domain.User;
import com.qin.travel.service.AddressService;
import com.qin.travel.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderService orderService;
    @Autowired
    private AddressDao addressDao;

    //保存订单数据
    @RequestMapping("/save")
    public ResultInfo save(HttpSession session, int aid){
        //根据session获得用户的登录数据
        User user = (User) session.getAttribute("user");
        //根据用户选择的地址，获取到address对象
        Address address = addressDao.findByAid(aid);
        //调用业务层，保存订单到数据库
        Order order = orderService.addOrder(user, address);
        return new ResultInfo(true,order);
    }
}

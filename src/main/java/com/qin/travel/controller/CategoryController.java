package com.qin.travel.controller;

import com.qin.travel.domain.Category;
import com.qin.travel.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/category", produces = "application/json;charset=utf-8")
public class CategoryController {
    @Autowired
    private CategoryService categoryService;

    @RequestMapping("/findAll")
    public List<Category> findAll() {
        return categoryService.findAll();
    }
}

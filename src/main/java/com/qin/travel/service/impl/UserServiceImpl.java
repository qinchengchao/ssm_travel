package com.qin.travel.service.impl;

import com.qin.travel.dao.UserDao;
import com.qin.travel.domain.ResultInfo;
import com.qin.travel.domain.User;
import com.qin.travel.service.UserService;
import com.qin.travel.utils.Md5Utils;
import com.qin.travel.utils.SmsUtils;
import com.qin.travel.utils.UuidUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.TimeUnit;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDao userDao;
    @Autowired
    private RedisTemplate redisTemplate;

    //注册业务
    @Override
    public ResultInfo save(User user, String code) {
        //1.先判断用户名是否可以使用
        User byName = userDao.findByName(user.getUsername());
        if (byName != null) {
            return new ResultInfo(false, "用户名已被占用");
        }
        User telephone = userDao.findTelephone(user.getTelephone());
        if (telephone != null) {
            return new ResultInfo(false, "手机号已被占用");
        }
        //对验证码进行判断
        //1.得到保存在缓存中的验证码数据
        String codeTest = (String) redisTemplate.opsForValue().get("code_" + user.getTelephone());
        //2.判断数据是否已过期
        if (codeTest == null) {
            return new ResultInfo(false, "验证码已过期");
        }
        //判断验证码是否相同
        if (!code.equalsIgnoreCase(codeTest)) {
            return new ResultInfo(false, "验证码有误");
        }

        //到这里，说明可以进行注册，需要对密码进行加密
        //调用uuid工具类，获取随机的字符串
        String simpleUuid = UuidUtils.simpleUuid();
        //将加上的盐也设置到user中
        user.setSalt(simpleUuid);
        //获得用户输入的账户名和密码
        String username = user.getUsername();
        String password = user.getPassword();
        //进行加盐处理
        String salt = username + password + simpleUuid;
        //调用MD5进行加密
        String pwd = Md5Utils.getMd5(salt);
        //将加密后的密码写入user对象中
        user.setPassword(pwd);
        //调用接口，执行SQL语句，写入数据库
        Integer save = userDao.save(user);


        if (save > 0) {
            return new ResultInfo(true, "注册成功");
        } else {
            return new ResultInfo(false, "注册失败");
        }

    }

    //验证用户名
    @Override
    public User findByName(String username) {

        return userDao.findByName(username);

    }

    //
    @Override
    public boolean findByTelephone(String telephone) {
        User byTelephone = userDao.findTelephone(telephone);
        if (byTelephone == null) {
            return true;
        }
        return false;
    }

    //发送验证码短信，并将验证码信息保存到缓存中，有效时间20秒
    @Override
    public ResultInfo sendSMS(String telephone) {
        //接收到传入的手机号码
        //调用工具类，产生一个随机的验证码
        String random = RandomStringUtils.randomNumeric(6);

        String send = SmsUtils.send(telephone, "黑马旅游网", "SMS_205126318", random);
        System.out.println(random);
//        String send = "OK";
        System.out.println(send);


        if ("OK".equalsIgnoreCase(send)) {
            //发送成功返回true和发送的验证码
            ResultInfo resultInfo = new ResultInfo(true, "发送验证码成功");
            //将验证码数据保存到redis中
            //设置保存的数据有效期为20秒
            redisTemplate.opsForValue().set("code_" + telephone, random, 60, TimeUnit.SECONDS);
            return resultInfo;
        } else {
            return new ResultInfo(false);
        }
    }

    //根据用户名发送登录验证码方法,
    @Override
    public ResultInfo sendMessage(String username) {
        if (username == null || username.trim() == "") {
            return new ResultInfo(false, "请输入用户名");
        }
        //首先，查询用户名是否存在
        User user = userDao.findByName(username);
        if (user == null) {
            return new ResultInfo(false, "用户名不存在");
        }
        //得到用户的手机号码
        String telephone = user.getTelephone();
        //调用发送短信方法，
        //调用工具类，产生一个随机的验证码
        String random = RandomStringUtils.randomNumeric(6);
//        String send = SmsUtils.send(telephone, "黑马旅游网", "SMS_205126318", random);
        System.out.println(random);
        String send = "OK";
        System.out.println(send);
        //判断是否发送成功
        if ("OK".equalsIgnoreCase(send)) {
            //发送成功返回true和发送的验证码
            ResultInfo resultInfo = new ResultInfo(true, "发送验证码成功");
            //将验证码数据保存到redis中
            //设置保存的数据有效期为20秒
            redisTemplate.opsForValue().set("login_" + telephone, random, 60, TimeUnit.SECONDS);
            return resultInfo;
        } else {
            return new ResultInfo(false, "短信发送失败");
        }

    }

    //用户登录方法

    @Override
    public ResultInfo loginUser(Map<String, String> param, User user) {
        //获得输入的验证码
        String code = param.get("code");
        //判断验证码是否正确
        if (code == null) {
            return new ResultInfo(false, "验证码为空");
        }
        //获取缓存中的验证码
        String co = (String) redisTemplate.opsForValue().get("login_" + user.getTelephone());
        if (!code.equalsIgnoreCase(co)) {
            return new ResultInfo(false, "验证码过期");
        }
        //获得传入的账户名
        String username = param.get("username");
        //获得登录密码
        String password = param.get("password");
        //获得盐
        String salt = user.getSalt();
        //对登录用户的密码进行加盐,加密处理
        String md5 = Md5Utils.getMd5(username + password + salt);
        //判断密码是否相同
        if (!md5.equals(user.getPassword())) {
            return new ResultInfo(false, "密码不正确");
        }
        return new ResultInfo(true);
    }
}

package com.qin.travel.dao;

import com.qin.travel.domain.Route;
import com.qin.travel.domain.RouteImg;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

public interface RouteDao {

    //根据cid和和名称进行分页查询
    List<Route> findRoute(@Param("cid") Integer cid, @Param("rname") String rname);

    //根据rid查询旅游线路，分类，和商家的map对象
    @Select("SELECT *  FROM tab_route r \n" +
            "INNER JOIN tab_category c ON c.cid=r.`cid`\n" +
            "INNER JOIN tab_seller s ON r.sid = s.sid WHERE rid=#{rid}\n")
    Map<String, Object> findRouteByRid(Integer rid);

    //根据rid查询指定线路的所有图片
    @Select("select * from tab_route_img where rid=#{rid}")
    List<RouteImg> findRouteImgByRid(Integer rid);
}
package com.qin.travel.service;

import com.qin.travel.domain.Address;

import java.util.List;

public interface AddressService {

    List<Address> findAddressByUid(Integer uid);
}

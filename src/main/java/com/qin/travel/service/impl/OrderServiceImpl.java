package com.qin.travel.service.impl;

import com.qin.travel.dao.AddressDao;
import com.qin.travel.dao.OrderDao;
import com.qin.travel.dao.OrderItemDao;
import com.qin.travel.domain.*;
import com.qin.travel.service.OrderService;
import com.qin.travel.utils.CartUtils;
import com.qin.travel.utils.UuidUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Date;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderDao orderDao;

    @Autowired
    private OrderItemDao orderItemDao;


    @Autowired
    private CartUtils cartUtils;

    //保存order到数据库
    //保存orderItem到数据库
    @Override
    public Order addOrder(User user, Address address) {
        //获取到用户的购物车
        Cart cart = cartUtils.getCart(user);
        //根据购物车的数据，设置orderItem中的数据

        //订单
        Order order = new Order();
        //设置订单的数据
        //给购物车设置id
        order.setOid(UuidUtils.simpleUuid());
        //设置订单的下单时机
        order.setOrdertime(new Date());
        //设置订单的总金额
        order.setTotal(cart.getCartTotal());
        //设置支付状态
        order.setState(0);
        //设置订单的地址
        order.setAddress(address.getAddress());
        //设置订单的地址的联系人
        order.setContact(address.getContact());
        //设置订单的手机号
        order.setTelephone(address.getTelephone());
        //设置订单的uid
        order.setUid(user.getUid());
        //将订单数据保存到数据库中
        orderDao.addOrder(order);

        /*=============================================*/
        //订单项
        OrderItem orderItem = new OrderItem();
        //获取购物车中的购物项集合
        Collection<CartItem> cartItems = cart.getCartItem().values();
        //根据购物项设置值订单项
        for (CartItem cartItem : cartItems) {
            //设置时间
            orderItem.setItemtime(new Date());
            //设置状态
            orderItem.setState(0);
            //设置数量
            orderItem.setNum(cartItem.getNum());
            //设置小计
            orderItem.setSubtotal(cartItem.getSubTotal());
            //设置订单项所属的订单的id
            orderItem.setOid(order.getOid());
            //设置rid
            orderItem.setRid(cartItem.getRoute().getRid());
            //将数据设置完毕后的对象保存到数据库中
            orderItemDao.AddOrderItem(orderItem);
        }

        //清空掉购物车
        cartUtils.removeCart(user);

        return order;
    }
}

package com.qin.travel.dao;

import com.qin.travel.domain.Category;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface CategoryDao {

    @Select("SELECT * FROM tab_category order BY cid")
    List<Category> findAll();
}

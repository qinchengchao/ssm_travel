package com.qin.travel.dao;

import com.qin.travel.domain.ResultInfo;
import com.qin.travel.domain.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;

public interface UserDao {

    //根据用户名查询用户
    @Select("select * from tab_user where username=#{username} ")
    User findByName(String username);

    //根据手机号查询用户
    @Select("select * from tab_user where telephone=#{telephone}")
    User findTelephone(String telephone);

    //注册方法
    @Insert("insert into tab_user values(null,#{username},#{password},#{telephone},#{salt})")
    Integer save(User user);


}

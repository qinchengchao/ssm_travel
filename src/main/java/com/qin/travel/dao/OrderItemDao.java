package com.qin.travel.dao;

import com.qin.travel.domain.OrderItem;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Update;

public interface OrderItemDao {
    //保存订单项
    @Insert("INSERT INTO tab_orderitem(itemtime,state,num,subtotal,rid,oid) VALUES(#{itemtime},#{state},#{num},#{subtotal},#{rid},#{oid})")
    Integer AddOrderItem(OrderItem orderItem);


    //更新订单项
    @Update("update tab_orderitem set state=1 where oid=#{oid}")
    void updateOrderItem(String oid);
}

package com.qin.travel.controller;

import com.github.pagehelper.PageInfo;
import com.qin.travel.domain.ResultInfo;
import com.qin.travel.domain.Route;
import com.qin.travel.service.RouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/route", produces = "application/json;charset=utf-8")
public class RouteController {

    @Autowired
    private RouteService routeService;

    //分页查询
    @RequestMapping("/findRoute")
    public PageInfo findRoute(Integer cid,
                              Integer pageNum,
                              String rname) {
        System.out.println("查询前");
        //判断
        if (rname != "" && rname != null) {
            cid = 0;
        }

        //调用业务，进行查询
        PageInfo<Route> pageInfo = routeService.findRoute(pageNum, cid, rname);

        return pageInfo;
    }

    //详情查询
    @RequestMapping("/findRouteByRid")
    public Route findRouteByRid(Integer rid) {

        return routeService.findRouteByRid(rid);
    }
}

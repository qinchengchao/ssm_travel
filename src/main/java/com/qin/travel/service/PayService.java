package com.qin.travel.service;

import com.qin.travel.domain.Order;

import java.util.Map;

public interface PayService {

    //更新订单状态
    void updateOrder(Map<String,String> map);

    //根据oid查询order
    boolean selectOrder(String oid);
}

package com.qin.travel.service.impl;

import com.qin.travel.dao.CategoryDao;
import com.qin.travel.domain.Category;
import com.qin.travel.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {
    @Autowired
    private CategoryDao categoryDao;

    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public List<Category> findAll() {
        List<Category> categoryList = null;
        //先在缓存中查询
        categoryList = (List<Category>) redisTemplate.opsForValue().get("category");
        if (categoryList == null) {
            categoryList = categoryDao.findAll();
            redisTemplate.opsForValue().set("category", categoryList);
        }

        return categoryList;
    }
}

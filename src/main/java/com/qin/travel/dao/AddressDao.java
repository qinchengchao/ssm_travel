package com.qin.travel.dao;


import com.qin.travel.domain.Address;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface AddressDao {

    @Select("select * from tab_address where uid=#{uid}")
    List<Address> findAddressByUid(Integer uid);

    @Select("select * from tab_address where aid=#{aid}")
    Address findByAid(Integer aid);

}

package com.qin.travel.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CartItem {
    //购物项类为在购物车中每一条数据


    //包含了route对象，数量，价格小计
    private Route route;//线路详情
    //
    private Integer num;

    private Double subTotal;//价格小计

    //小计等于数量*价格
    public Double getSubTotal() {
        this.subTotal = 0.0;

        this.subTotal = num * route.getPrice();

        return subTotal;
    }
}

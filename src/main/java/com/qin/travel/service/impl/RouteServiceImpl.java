package com.qin.travel.service.impl;


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.qin.travel.dao.RouteDao;
import com.qin.travel.domain.Category;
import com.qin.travel.domain.Route;
import com.qin.travel.domain.RouteImg;
import com.qin.travel.domain.Seller;
import com.qin.travel.service.RouteService;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class RouteServiceImpl implements RouteService {
    @Autowired
    private RouteDao routeDao;

    @Override
    public PageInfo<Route> findRoute(Integer pageNum, Integer cid, String rname) {
        //设置当前页面的大小,参数为当前页码，和页面大小
        PageHelper.startPage(pageNum, 5);
        //调用接口方法，查询数据
        List<Route> routeList = routeDao.findRoute(cid, rname);
        System.out.println(routeList);
        //创建分页对象，储存数据,参数为需要暂示的数据
        PageInfo<Route> routePageInfo = new PageInfo<>(routeList);

        return routePageInfo;
    }

    //根据rid，获得对应线路的详情，
    @Override
    public Route findRouteByRid(Integer rid) {
        try {
            //调用接口，获得线路的类别，商家，线路
            Map<String, Object> routeMap = routeDao.findRouteByRid(rid);
            //调用接口，查询线路的图片
            List<RouteImg> imgList = routeDao.findRouteImgByRid(rid);
            //创建一个route对象
            Route route = new Route();
            //创建category对象,seller对象。
            Category category = new Category();
            Seller seller = new Seller();
            //将查询出的数据进行封装，
            BeanUtils.populate(category, routeMap);
            BeanUtils.populate(seller, routeMap);
            BeanUtils.populate(route, routeMap);
            //将得到的对象数据，封装到route中
            route.setSeller(seller);
            route.setCategory(category);
            route.setRouteImgList(imgList);

            return route;
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}

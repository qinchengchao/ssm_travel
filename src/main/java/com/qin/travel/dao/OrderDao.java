package com.qin.travel.dao;

import com.qin.travel.domain.Order;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

public interface OrderDao {
    //保存订单
    @Insert("INSERT INTO tab_order VALUES(#{oid},#{ordertime},#{total},#{state},#{address},#{contact},#{telephone},#{uid}) ")
    Integer addOrder(Order order);

    //根据订单状态
    @Update("update tab_order set state=1 where oid=#{oid}")
    void updateOrder(String oid);
    //根据oid查询对应的order
    @Select("select * from tab_order where oid=#{oid}")
    Order selectState(String oid);
}

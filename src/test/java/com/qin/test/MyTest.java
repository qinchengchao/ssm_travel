package com.qin.test;

import com.github.pagehelper.PageInfo;
import com.qin.travel.domain.ResultInfo;
import com.qin.travel.domain.Route;
import com.qin.travel.domain.User;
import com.qin.travel.service.RouteService;
import com.qin.travel.service.UserService;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class MyTest {

    @Autowired
    private UserService userService;
    @Autowired
    private RouteService routeService;

    @Test
    public void test1() {
        User user = new User();
        user.setUsername("aa");
        user.setPassword("123456");
        user.setTelephone("13138201575");
        ResultInfo save = userService.save(user, "");
        System.out.println(save);
    }

    @Test
    public void test2() {
        String s = RandomStringUtils.randomNumeric(6);
        System.out.println(s);
    }

    @Test
    public void test3() {
        PageInfo<Route> route = routeService.findRoute(1, 2, "");

        System.out.println(route);
    }

    @Test
    public void test4() {

        Route routeByRid = routeService.findRouteByRid(10);
        System.out.println(routeByRid);
    }
}

package com.qin.travel.controller;

import com.qin.travel.domain.*;
import com.qin.travel.service.RouteService;
import com.qin.travel.utils.CartUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.LinkedHashMap;

@RestController
@RequestMapping(path = "/cart", produces = "application/json;charset=utf-8")
public class CartController {
    @Autowired
    private CartUtils cartUtils;
    @Autowired
    private RouteService routeService;

    //添加商品到购物车
    @RequestMapping("/addCart")
    public ResultInfo addCart(HttpSession session, Integer rid, Integer num) {
        //首先获得用户的数据
        User user = (User) session.getAttribute("user");
        //直接通过工具类获取当前用户的购物车
        Cart cart = cartUtils.getCart(user);
        //遍历购物车中的购物项
        LinkedHashMap<String, CartItem> map = cart.getCartItem();
        //通过rid来获取购物车中相同的购物项，
        CartItem cartItem = map.get(rid);
        //判断购物车中是否存在相同的购物项
        if (cartItem == null) {
            //等于空，说明没有相同的route，需要自己创建对象，否则会报错，空指针异常
            cartItem = new CartItem();
            //调用业务方法，获得完整的route,则需要添加新的键值对
            Route route = routeService.findRouteByRid(rid);
            //创建一个新的CartItem,并将参数赋值
            cartItem.setRoute(route);
            cartItem.setNum(num);

            map.put(rid + "", cartItem);
        } else {
            //不等于空，则代表购物项中有相同的route，只徐亚将数量增加
            cartItem.setNum(cartItem.getNum() + num);
        }
        //将重新设置好的购物车项添加到会话域中，为回显
        session.setAttribute("cartItem", cartItem);
        //将修改后的Cart数据更新到Redis中
        cartUtils.setCart(user, cart);
        //返回数据
        return new ResultInfo(true, "添加成功");
    }

    //添加购物车回显
    @RequestMapping("/findCartItem")
    public ResultInfo findCartItem(HttpSession session) {
        //获取会话域的对象
        CartItem cartItem = (CartItem) session.getAttribute("cartItem");
        return new ResultInfo(true, cartItem);
    }

    //查看购物车
    @RequestMapping("/findAllCart")
    public ResultInfo findAllCart(HttpSession session) {
        //获得session中保存的登录数据
        User user = (User) session.getAttribute("user");
        //获得购物车
        Cart cart = cartUtils.getCart(user);

        return new ResultInfo(true, cart);
    }

    //删除购物车中数据
    @RequestMapping("/deleteCartItem")
    public ResultInfo deleteCartItem(Integer rid, HttpSession session) {
        //获取登录数据
        User user = (User) session.getAttribute("user");
        //根据rid找到cart中对应的cartItem
        Cart cart = cartUtils.getCart(user);
        LinkedHashMap<String, CartItem> cartItemMap = cart.getCartItem();
        //删除
        CartItem remove = cartItemMap.remove(rid+"");
        //更新Redis
        cartUtils.setCart(user,cart);

        return new ResultInfo(true, "已经删除",cart);
    }
}
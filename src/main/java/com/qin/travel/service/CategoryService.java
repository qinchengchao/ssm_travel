package com.qin.travel.service;

import com.qin.travel.domain.Category;

import java.util.List;

public interface CategoryService {

    List<Category> findAll();
}

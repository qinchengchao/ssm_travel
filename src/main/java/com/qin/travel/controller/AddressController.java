package com.qin.travel.controller;

import com.qin.travel.domain.Address;
import com.qin.travel.domain.Cart;
import com.qin.travel.domain.ResultInfo;
import com.qin.travel.domain.User;
import com.qin.travel.service.AddressService;
import com.qin.travel.utils.CartUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping(path = "/order",produces = "application/json;charset=utf-8")
public class AddressController {

    @Autowired
    private AddressService addressService;

    @Autowired
    private CartUtils cartUtils;

    @RequestMapping("/findAddress")
    public ResultInfo findAddress(HttpSession session) {
        //获取到session中的数据
        User user = (User) session.getAttribute("user");
        //根据登录数据中的uid获取用户的所有地址
        int uid = user.getUid();
        List<Address> addressList = addressService.findAddressByUid(uid);
        //获取用户在redis中的购物车数据
        Cart cart = cartUtils.getCart(user);
        //将地址集合与购物车数据封装成一个map集合返回给前端页面
        HashMap<String, Object> map = new HashMap<>();
        map.put("list", addressList);
        map.put("cart", cart);
        //将集合封装到消息结果对象中
        return new ResultInfo(true, map);

    }
}

package com.qin.travel.service;

import com.github.pagehelper.PageInfo;
import com.qin.travel.domain.Route;


public interface RouteService {

    //根据cid或者名字进行分页查询
    PageInfo<Route> findRoute(Integer pageNum, Integer cid, String cname);

    //根据rid，查询线路的详细信息
    Route findRouteByRid(Integer rid);
}

package com.qin.travel.service;

import com.qin.travel.domain.Address;
import com.qin.travel.domain.Order;
import com.qin.travel.domain.User;

public interface OrderService {

    Order addOrder(User user, Address address);
}

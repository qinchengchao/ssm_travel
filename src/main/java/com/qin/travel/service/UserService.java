package com.qin.travel.service;

import com.qin.travel.domain.ResultInfo;
import com.qin.travel.domain.User;

import java.util.Map;


public interface UserService {

    //注册
    ResultInfo save(User user, String code);

    //验证用户名
    User findByName(String Username);

    //验证手机号
    boolean findByTelephone(String telephone);

    //发送验证码短信
    ResultInfo sendSMS(String telephone);

    //根据用户名发送验证码方法,
    ResultInfo sendMessage(String username);

    //用户登录方法
    ResultInfo loginUser(Map<String ,String> param,User user);


}

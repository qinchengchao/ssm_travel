package com.qin.travel.controller;

import com.aliyuncs.http.HttpRequest;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.qin.travel.domain.ResultInfo;
import com.qin.travel.service.PayService;
import com.qin.travel.utils.PayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/pay")
public class PayController {
    @Autowired
    private PayService payService;

    @RequestMapping("/money")
    public String payPrice(String oid) {
        String url = PayUtils.createOrder(oid, 1);

        return url;
    }

    //微信支付的回调方法
    @RequestMapping("/payNotify")
    public Map<String, String> payNotify(HttpServletRequest request) throws IOException {


        //获取输入流
        ServletInputStream inputStream = request.getInputStream();
        //获取转换的对象
        XmlMapper xmlMapper = new XmlMapper();
        //将除回的数据转换为map,传回的数据中包含订单号
        Map<String, String> map = xmlMapper.readValue(inputStream, Map.class);
        //调用业务进行修改
        payService.updateOrder(map);
        //需要对微信支付返回数据，告知已经接收到数据
        Map<String, String> param = new HashMap<>();
        param.put("return_code", "SUCCESS");
        param.put("return_msg", "OK");
        return param;

    }

    @RequestMapping("/selectPay")
    public boolean selectPay(String oid) {
        return payService.selectOrder(oid);
    }
}
